# EKS with Calico

My boilerplate setup for an EKS cluster with Calico CNI.


## How To Deploy

This assumes that you wanted ssh keys and you use aws, and have run `make keygen`
in the case that you did. If you don't want ssh to the nodes then you will need
to remove the ssh config from the `cluster.yaml`


1. Deploy Control Plane without Nodes

```bash
make create-cluster-no-ng
```

Why is this important? Because we don't want to get the CNI config file from the
AWS CNI on all of the nodes. Calico will try to use it when we deploy it. And
then the only want to get rid of the AWS CNI is either destroy the nodes, and
let autoscaler (in this case because our `cluster.yaml` has it) or spin back up
new ones in the case where you don't have an autoscaler. Or ssh into each and
manually delete them.


2. Delete AWS CNI

```bash
make dl-aws-cni
```

This will get rid of the `aws-node` DaemonSet.


3. Create NodeGroups

```bash
make ng
```

This will use the `cluster.yaml` to deploy the NodeGroups. Now our cluster should
be whole. Now we can get rid of the first temporary NodeGroup.

```bash
make dl-tmp-ng
```

I should note, that the NodeGroups will fail and not become "Ready". This is
because the kubelet will fail to start because there is no CNI. That's ok, we
will create it in the last step and they will automatically become ready.


4. Deploy Calico

```bash
make calico
```


## Improvements

I'm trying to get this to mostly be `make up`. I do need to iron out some kinks.
Also there needs to be proper wait until ready checks of some kind. Before
jumping to the next command in the `make up` alias. Kubernetes can sometimes
be "deployed" but take some time before it's "Ready" and you can deploy to it.


## Extras In Makefile

Yes, I know there is some extra targets in my `Makefile`. This is mostly for
convenience because with kubernetes release 1.24+ you need extra CSI EBS policies
and what not, in order to get things working right. I probably won't outline
those necessities in this repo. Though I probably should. I wanted to mostly
focus on the "Calico" part.


## Other Assumptions

There are quite a bit of assumptions with this. Again, I really just wanted to
focus on the "How To Setup Calico On EKS". Some other would be you have the
cluster config, you have already pulled the helm chart and have the version in
this `Makefile`, etc. I'm sure there are more I'm not listing.


