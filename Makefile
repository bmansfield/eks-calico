CLUSTER ?= my-cluster
REGION ?= us-west-2


all: keygen create-cluster docker-secret

default: create-cluster

keygen:
	ssh-keygen \
		-t rsa \
		-b 4096 \
		-C $(CLUSTER) \
		-f $(CLUSTER)
	aws ec2 \
		import-key-pair \
		--key-name $(CLUSTER)-eks \
		--public-key-material fileb://$(CLUSTER).pub

rm-key:
	aws ec2 \
		delete-key-pair \
		--key-name $(CLUSTER)-eks
	rm $(CLUSTER)
	rm $(CLUSTER).pub

add-csi-sa:
	eksctl create \
		iamserviceaccount \
		--name ebs-csi-controller-sa \
		--namespace kube-system \
		--cluster $(CLUSTER) \
		--region=$(REGION) \
		--attach-policy-arn arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy \
		--approve \
		--role-only \
		--role-name AmazonEKS_EBS_CSI_DriverRole

create-cluster-no-ng:
	eksctl create \
		cluster \
		-f $(CLUSTER).yaml \
		--kubeconfig=./$(CLUSTER)-kubeconfig.yml \
		--without-nodegroup \
		--verbose 5

create-cluster:
	eksctl create \
		cluster \
		-f $(CLUSTER).yaml \
		--kubeconfig=./$(CLUSTER)-kubeconfig.yml \
		--verbose 5

dl-aws-cni:
	kubectl delete ds -n kube-system aws-node

calico:
	helm upgrade \
		-i calico \
		-n tigera-operator \
		-f calico-eks.yaml \
		--create-namespace \
		--version v3.25.1 \
		projectcalico/tigera-operator

create-nodegroup create-ng ng:
	eksctl create \
		nodegroup \
		-f $(CLUSTER).yaml \
		--verbose 5

up: create-cluster-no-ng dl-aws-cni create-ng calico

clean cleanup teardown: delete-cluster rm-key
	rm ./$(CLUSTER)-kubeconfig.yml || true
	rm ./kubeconfig.yml.eksctl.lock || true

del delete delete-cluster down rm:
	eksctl delete \
		cluster \
		--name $(CLUSTER) \
		--verbose 5

update upgrade:
	eksctl upgrade \
		cluster \
		-f $(CLUSTER).yaml \
		--approve

docker-secret:
	kubectl create \
		secret \
		generic \
		registrykey \
		--from-file=.dockerconfigjson=dockerregistrycreds.json \
		--type=kubernetes.io/dockerconfigjson \
		--dry-run \
		-o yaml | \
		kubectl apply -f -

ls list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'

.PHONY: keygen rm-key add-csi-sa create-cluster create-cluster-no-ng dl-aws-cni calico create-nodegroup create-ng ng up clean cleanup teardown del delete delete-cluster down rm update upgrade docker-secret ls list

